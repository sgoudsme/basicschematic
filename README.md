# Start project.

Basic schematic. 5x5 cm pcb with Atmega328p and the option to add an ethernet or CAN shield.

Compatible with CAN shield: TJA1050  
Compatible with Ethernet shield: USR-ES1

LDO to have maximal 12V input. The VDD of the circuit is 3.3v!  
Except for the fuse, all parts are available at the JLCPCB Assembly service.

Can be used as a Kicad Template. Use "Create project from template" and navigate to this folder to start.

Screenshot:  
![alt text][screenshot]

[screenshot]: Screenshot.png "Screenshot"
